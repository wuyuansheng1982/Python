import cv2
path = r'original.jpg'
image_path = cv2.imread(path)
grey_image = cv2.cvtColor(image_path, cv2.COLOR_BGR2GRAY)
cv2.imshow('Image', grey_image)
invert = cv2.bitwise_not(grey_image)
blur = cv2.GaussianBlur(invert, (11, 11), cv2.BORDER_DEFAULT)
invertedblur = cv2.bitwise_not(blur)
sketch = cv2.divide(grey_image, invertedblur, scale = 256.0)
cv2.imwrite('sketch.png', sketch)
