# simple web server
python3 -m http.server

# pretty printing
from pprint import pprint

my_dict = {'name': 'Yasoob', 'age': 'undefined', 'personality': 'awesome'}
pprint(my_dict)

cat file.json | python -m json.tool


python -m cProfile my_script.py

# csv to json
python -c "import csv,json;print json.dumps(list(csv.reader(open('csv_file.csv'))))"

# list flattening
a_list = [[1, 2], [3, 4], [5, 6]]
print(list(itertools.chain.from_iterable(a_list)))