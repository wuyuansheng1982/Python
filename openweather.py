import requests, json, pprint
queryurl = 'http://api.openweathermap.org/data/2.5/weather?id=1886760'
queryurl = queryurl + '&appid=63bee18639d28a8690fb60e553f8a0af'

response = requests.get(queryurl)
response.raise_for_status()

weatherData = json.loads(response.text)
pprint.pprint(weatherData)

print('Current weather in Suzhou:')
print(weatherData['weather'][0]['main'], '-', weatherData['weather'][0]['description'])
