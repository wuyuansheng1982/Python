from collections import defaultdict

colors = (
    ('Yasoob', 'Yellow'),
    ('Ali', 'Blue'),
    ('Arham', 'Green'),
    ('Ali', 'Black'),
    ('Yasoob', 'Red'),
    ('Ahmed', 'Silver')
)
fav = defaultdict(list)

for name, color in colors:
    fav[name].append(color)

print(fav)


# https://gist.github.com/hrldcpr/2012250
# It simply says that a tree is a dict whose default values are trees.
# Now we can create JSON-esque nested dictionaries without explicitly creating sub-dictionaries—they 
# magically come into existence as we reference them:

tree = lambda: defaultdict(tree)
some_dict = tree()
some_dict['colors']['fav'] = "yellow"

import json
print(json.dumps(some_dict))