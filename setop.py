some_list = ['a', 'b', 'c', 'b', 'd', 'm', 'n', 'n']

dup = [x for x in some_list if some_list.count(x) > 1]
print(dup)
duplicates = set([x for x in some_list if some_list.count(x) > 1])
print(duplicates)

test = set(some_list)
print(test)

# Intersection #
valid = set(['yellow', 'red', 'blue', 'green', 'black'])
input_set = set(['red', 'brown'])
print(input_set.intersection(valid))

# Difference
valid = set(['yellow', 'red', 'blue', 'green', 'black'])
input_set = set(['red', 'brown'])
print(input_set.difference(valid))
print(valid.difference(input_set))