try:
    file = open('test.txt', 'rb')
except IOError as e:
    print('An IOError occured. {}'.format(e.args[-1]))


try:
    file = open('test.txt', 'rb')
except EOFError as e:
    print("An EOF error occured.")
    raise e
except IOError as e:
    print("An IOError occured.")
    raise e
except Exception as e:
    print("An error occured")
    raise e
else:
    print("else no excpetion")
finally: 
    print("No exception")
