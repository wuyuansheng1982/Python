# you can think of namedtuples like dictionaries 
# but unlike dictionaries they are immutable
# we can access members of a tuple just by their name using a .
# A named tuple has two required arguments. tuple name, tuple field names

from collections import namedtuple
Animal = namedtuple('Animal', 'name age type')
perry = Animal(name='perry', age=31, type='cat')
print(perry)
print(perry.name)
print(perry._asdict())