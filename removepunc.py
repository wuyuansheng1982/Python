import re, string
def removePunctuation(text):
   regex = re.compile('[%s]' % re.escape(string.punctuation))
   text=regex.sub('', text)
   return text

print(removePunctuation('Hi, you!'))
print(removePunctuation(' No under_score!'))
